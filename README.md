# Koha Plugin: Pending Reserves V2

v0.93,
- added ISBN, size and pages to the title row.
- duplicated filtering row to the top of the table

v0.92,
- removed unnecessary and blocking for filtering "span" tag

v0.91, pre-release
- Split "Earliest hold date" column into "Earliest hold date" and "Earliest hold at" columns.

v0.9, pre-release
- Full duplicate of Koha's "Holds to pull" report (based on pendingreserves.pl) with two columns added: Item CCODE and Item Notes. More to be added in "release", after confirmation from librarians.

Contact me to Petro <properperl@gmail.com> with requests to add more columns,
or to remove a few, to compress a few into one or to print extra info in
existing column, if needed.
